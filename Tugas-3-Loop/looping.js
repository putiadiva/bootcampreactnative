// No 1
console.log("LOOPING PERTAMA")
var i = 1
while (i<=10) {
    console.log((i*2).toString() + " - I Love Coding")
    i++
}
console.log()

console.log("LOOPING KEDUA")
var j = 10
while (j>=1) {
    console.log((j*2).toString() + " - I Love Coding")
    j--
}
console.log()

// No 2
for(var k=1; k<=20; k++){
    if(k%2 == 0){
        console.log(k.toString() + " - Berkualitas")
    } else if (k%3 == 0){
        console.log(k.toString() + " - I Love Coding")
    } else {
        console.log(k.toString() + " - Santai")
    }
}
console.log()

// No 3
for(var a=0; a<4; a++){
    for(var b=0; b<8; b++){
        process.stdout.write("#")
    }
    console.log()
}
console.log()

// No 4
for(var p=0; p<=7; p++){
    for(var q=0; q<p; q++){
        process.stdout.write("#")
    }
    console.log()
}
console.log()

// No 5
for(var x= 1; x<=8; x++){
    var count = 0
    if(x%2==0){
        while (count<4) {
            process.stdout.write("#")
            process.stdout.write(" ")
            count++
        }
    } else {
        while (count<4) {
            process.stdout.write(" ")
            process.stdout.write("#")
            count++
        }
    }
    console.log()
}
console.log()
