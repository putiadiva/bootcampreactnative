let readBooks = require('./callback.js')
 
let books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

let timeAvailable = 10000
readBooks(timeAvailable, books[0], function(time) {
    timeAvailable -= books[0].timeSpent
    readBooks(timeAvailable, books[1], function(time){
        timeAvailable -= books[1].timeSpent
        readBooks(timeAvailable, books[2], function(time){
            return time
        })
    })
})
