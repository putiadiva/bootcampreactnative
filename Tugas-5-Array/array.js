// No 1
function range(startNum, finishNum){
    var arrResult = []
    if(startNum == null || finishNum == null){
        return -1
    }

    if(startNum<=finishNum){
        for(var i=startNum; i<=finishNum; i++){
            arrResult.push(i)
        }
        
    } else {
        for(var i=startNum; i>=finishNum; i--){
            arrResult.push(i)
        }
    }
    return arrResult
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 
console.log()

// No 2
function rangeWithStep(startNum, finishNum, step) {
    var arrResult2 = []
    if(startNum<=finishNum){
        for(var i=startNum; i<=finishNum; i+=step){
            arrResult2.push(i)
        }
        
    } else {
        for(var i=startNum; i>=finishNum; i-=step){
            arrResult2.push(i)
        }
    }
    return arrResult2
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 
console.log()

// No 3
function sum(startNum, finishNum, step=1){
    var sumResult = 0
    if(finishNum == null){
        if(startNum == null){
            return 0
        }
        return startNum
    }

    if(startNum<=finishNum){
        for(var i=startNum; i<=finishNum; i+=step){
            sumResult += i
        }
        
    } else {
        for(var i=startNum; i>=finishNum; i-=step){
            sumResult += i
        }
    }
    return sumResult
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 
console.log()

// No 4
function dataHandling(arr){
    for(var n=0; n<arr.length; n++){
        console.log("Nomor ID : " + arr[n][0])
        console.log("Nama lengkap : " + arr[n][1])
        console.log("TTL : " + arr[n][2] + " " + arr[n][3])
        console.log("Hobi : "+ arr[n][4])
        console.log()
    }
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

dataHandling(input)
console.log()

// No 5
function balikKata(kata){
    balikResult = ""
    for(var i=kata.length-1; i>=0; i--){
        balikResult += kata[i]
    }
    return balikResult
}
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I
console.log()

// No 6
function dataHandling2(arr){
    arr.splice(4, 1, "Pria", "SMA Internasional Metro")
    arr[1] = arr[1] + " Elsharawy"
    arr[2] = "Provinsi " + arr[2]

    var tanggal = arr[3]
    tanggal = tanggal.split("/")
    var bulan = tanggal[1]
    var tanggal_formatted = tanggal.join("-")
    var tanggal_sort = tanggal.sort(function(a,b){return b-a})

    switch (bulan) {
        case "01":
            var bulan2 = "Januari"
            break;
        case "02":
            var bulan2 = "Februari"
            break;
        case "03":
            var bulan2 = "Maret"
            break;
        case "04":
            var bulan2 = "April"
            break;
        case "05":
            var bulan2 = "Mei"
            break;
        case "06":
            var bulan2 = "Juni"
            break;
        case "07":
            var bulan2 = "Juil"
            break;
        case "08":
            var bulan2 = "Agustus"
            break;
        case "09":
            var bulan2 = "September"
            break;
        case "10":
            var bulan2 = "Oktober"
            break;
        case "11":
            var bulan2 = "November"
            break;
        case "12":
            var bulan2 = "Desember"
            break;
        default:
            break;
    }
    var nama = arr[1]
    if(nama.length>15){
        nama = nama.slice(0,14)
    }

    console.log(arr)
    console.log(bulan2)
    console.log(tanggal_sort)
    console.log(tanggal_formatted)
    console.log(nama)

}
var input2 = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]
dataHandling2(input2)