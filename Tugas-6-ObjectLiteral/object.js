// No 1
console.log("No. 1")
var now = new Date()
var thisYear = now.getFullYear()

function arrayToObject(arr){
    var n = arr.length
    for(var i=0; i<n; i++){
        var obj = {
            firstname : arr[i][0],
            lastname : arr[i][1],
            gender : arr[i][2],
            age : thisYear - arr[i][3]
        }
        if(obj.age<0 || obj.age==NaN || arr[i][3] == null){
            obj.age = "Invalid Birth Year"
        }
        process.stdout.write((i+1).toString() + ". " + obj.firstname + obj.lastname + " : ")
        console.log(obj)
    }
}

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people)
console.log("---")
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2)


// No 2
console.log()
console.log("No. 2")
function shoppingTime(memberId, money){
    var stacattu = {
        nama : "Sepatu Stacattu",
        harga : 1500000
    }
    var zoro = {
        nama : "Baju Zoro",
        harga : 500000
    }
    var hnn = {
        nama : "Baju HnN",
        harga : 250000
    }
    var uniklooh = {
        nama : "Sweater Uniklooh",
        harga : 175000
    }
    var casing = {
        nama : "Casing Handphone",
        harga : 50000
    }
    var arr_barang = [stacattu, zoro, hnn, uniklooh, casing]

    var customer = {
        memberId : memberId,
        money : money,
        listPurchased : [],
        changeMoney : 0
    }

    var i = 0
    var moneySpent = 0
    while (i<5) {
        if (customer.money > arr_barang[i].harga && moneySpent <= customer.money) {
            customer.listPurchased.push(arr_barang[i].nama)
            moneySpent += arr_barang[i].harga
        }
        i++
    }
    
    customer.changeMoney = customer.money - moneySpent

    if(memberId == "" || memberId == null){
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    } else if(money < 50000){
        return "Mohon maaf, uang tidak cukup"
    } else {
        return customer
    }
}

console.log(shoppingTime('1820RzKrnWn08', 2475000))
console.log(shoppingTime('82Ku8Ma742', 170000))
console.log(shoppingTime('', 2475000))
console.log(shoppingTime('234JdhweRxa53', 15000))
console.log(shoppingTime())

// No 3
console.log()
console.log("No. 3")
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var harga = 2000
    var n = arrPenumpang.length
    var arrOfObj = []
    for(var i=0; i<n; i++){
        var obj = {
            penumpang : arrPenumpang[i][0],
            naikDari : arrPenumpang[i][1],
            tujuan : arrPenumpang[i][2],
            bayar : ((rute.indexOf(arrPenumpang[i][2]))-(rute.indexOf(arrPenumpang[i][1])))*harga
        }
        arrOfObj.push(obj)
    }
    return arrOfObj
}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]))