// No 1
class Animal{
    constructor(name){
        this.name = name
        this.legs = 4
        this.cold_blooded = false
    }

    get animal_name(){
        return this.name
    }

    get animal_legs(){
        return this.legs
    }

    get animal_cold_blooded(){
        return this.cold_blooded
    }
}

class Frog extends Animal{
    constructor(name){
        super(name)
    }

    jump(){
        console.log("hop hop")
    }
}

class Ape extends Animal{
    constructor(name){
        super(name)
        this.legs = 2
    }
    yell(){
        console.log("Auooo")
    }
}

var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 
console.log()

// No 2
class Clock {
    constructor({ template }){
        //var timer
        var date = new Date()
        this.hour = date.getHours()
        this.minute = date.getMinutes()
        this.second = date.getSeconds()
    }

    start(){
        console.log(this.hour, this.minute, this.second)
    }
}

var clock = new Clock({template: 'h:m:s'})
clock.start()