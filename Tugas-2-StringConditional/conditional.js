// Soal if else
var nama = "John";
var peran = "";

if (nama == ""){
    console.log("Nama harus diisi!");

} else if (peran == ""){
    console.log("Halo, " + nama + ". Pilih peranmu untuk memulai game!");

} else {
    console.log("Selamat datang di dunia Werewolf, " + nama + "!");

    if (peran.toLowerCase() == "penyihir"){
        console.log("Halo Penyihir " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!");

    } else if (peran.toLowerCase() == "guard"){
        console.log("Halo Guard " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.");

    } else if (peran.toLowerCase() == "werewolf"){
        console.log("Halo Werewolf " + nama + ", Kamu akan memakan mangsa setiap malam!");
    }
}

// Soal switch case
var hari = 21; 
var bulan = 1; 
var tahun = 1945;

switch (bulan) {
    case 1:
        console.log(hari.toString() + " Januari " + tahun.toString());
        break;
    case 2:
        console.log(hari.toString() + " Februari " + tahun.toString());
        break;
    case 3:
        console.log(hari.toString() + " Maret " + tahun.toString());
        break;
    case 4:
        console.log(hari.toString() + " April " + tahun.toString());
        break;
    case 5:
        console.log(hari.toString() + " Mei " + tahun.toString());
        break;
    case 6:
        console.log(hari.toString() + " Juni " + tahun.toString());
        break;
    case 7:
        console.log(hari.toString() + " Juli " + tahun.toString());
        break;
    case 8:
        console.log(hari.toString() + " Agustus " + tahun.toString());
        break;
    case 9:
        console.log(hari.toString() + " September " + tahun.toString());
        break;
    case 10:
        console.log(hari.toString() + " Oktober " + tahun.toString());
        break;
    case 11:
        console.log(hari.toString() + " November " + tahun.toString());
        break;
    case 12:
        console.log(hari.toString() + " Desember " + tahun.toString());
        break;
    default:
        break;
}