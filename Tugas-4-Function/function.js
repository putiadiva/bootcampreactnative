// No 1
function teriak(){
    return "Halo Sanbers!"
}

console.log(teriak())
console.log()

// No 2
function kalikan (x,y){
    return x*y
}

var num1 = 12
var num2 = 4
var hasilkali = kalikan(num1, num2)
console.log(hasilkali)
console.log()

// No 3
function introduce(name, age, adress, hobby){
    return "Nama saya " + name + ", umur saya " + age + " tahun, alamat saya di " + adress + ", dan saya punya hobby yaitu " + hobby + "!"
}

var myName = "Agus"
var myAge = 30
var myAddress = "Jln. Malioboro, Yogyakarta"
var myHobby = "Gaming"
var perkenalan = introduce(myName, myAge, myAddress, myHobby)
console.log(perkenalan)
console.log()

